%% Quintic Spline Ribbon implementation
% Copyright (C) 2019  Jeroen van Nugteren

% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

%% bezier function
function [x,v,a] = bezier(t,P)
	% get sizes
	num_dim = size(P,1);    
	n = size(P,2)-1;
	
	% position
	x = zeros(num_dim,length(t));
	v = zeros(num_dim,length(t));
	a = zeros(num_dim,length(t));
	
	% walk over the source points
	for i=0:n
		% calculate binomial coefficient
		nk = factorial(n)/(factorial(i)*factorial(n-i));
		
		% walk over dimensions
		for d=1:num_dim
			% coordinates
			x(d,:) = x(d,:) + nk*P(d,i+1)*(1.0-t).^(n-i) .* t.^i;

			% velocity (first derivative)
			v(d,:) = v(d,:) + nk*P(d,i+1)*(n*t-i).*(1.0-t).^(n-i).*t.^(i-1)./(t-1);

			% acceleration (second derivative)
			a(d,:) = a(d,:) + nk*P(d,i+1)*((-1+i)*i*(1-t).^(-i+n).*t.^(-2+i) - ...
				2*i*(-i+n).*(1-t).^(-1-i+n).*t.^(-1+i) + (-1-i+n).*(-i+n).*(1-t).^(-2-i+n).*t.^i);
		end
	end
end

