%% Quintic Spline Ribbon implementation
% Copyright (C) 2019  Jeroen van Nugteren

% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

%% Input settings
% Cloverleaf Case
% number of points
num_points = 50;
height = 24e-3;
wcable = 12e-3;
dcable = 1.2e-3;
elltrans = 10e-3;

% start point
X0 = [0;-elltrans;0]; X1 = [elltrans;0;height];
L0 = [0;1;0]; L1 = [1;0;0];
N0 = [1;0;0]; N1 = [0;-1;0];
strength1 = 30e-3;
strength2 = 30e-3;
strength3 = 10e-3;
strength4 = 10e-3;

% % Racetrack End Case
% % number of points
% num_points = 50;
% height = 14e-3;
% wcable = 12e-3;
% dcable = 1.2e-3;
% elltrans = 10e-3;

% % start point
% X0 = [0;-elltrans;0]; X1 = [0.04;-elltrans;height];
% L0 = [0;1;0]; L1 = [0;-1;0];
% N0 = [-1;0;0]; N1 = [1;0;0];
% strength1 = 30e-3;
% strength2 = 30e-3;
% strength3 = 10e-3;
% strength4 = 10e-3;

% % Cosine Theta End Case
% % number of points
% num_points = 120;
% height = 14e-3;
% wcable = 12e-3;
% dcable = 1.2e-3;
% elltrans = 10e-3;
% angle = 20*2*pi/360;
% radius = 0.07/2;

% % start point
% X0 = [0;0;0]; X1 = [2*radius;0;0];
% L0 = [0;1;0]; L1 = [0;-1;0];
% N0 = [-sin(angle);0;-cos(angle)]; N1 = [sin(angle);0;-cos(angle)];
% strength1 = radius;
% strength2 = radius;
% strength3 = radius;
% strength4 = radius;

%% calculation of splines
% control points for the quintic spline
P1 = X0+L0*elltrans; 
P2 = X0+L0*elltrans+L0*strength1; 
P3 = X0+L0*elltrans+L0*(strength1+strength2)-N0*strength3;
P4 = X1-L1*elltrans-L1*(strength1+strength2)-N1*strength4;
P5 = X1-L1*elltrans-L1*strength1;
P6 = X1-L1*elltrans;
Ph = [P1,P2,P3,P4,P5,P6];

% set time
t = linspace(1e-9,1-1e-9,num_points);

% calculate spline
[X,V,A] = bezier(t,Ph); 

% normalize
A = bsxfun(@rdivide,A,sqrt(sum(A.*A,1)));
V = bsxfun(@rdivide,V,sqrt(sum(V.*V,1)));

%% create darboux generators
% radial vector
gen = cross(A(:,2:end),A(:,1:end-1));

% extend
gen = ([gen,gen(:,end)] + [gen(:,1),gen])/2;

% normalized generators
genNorm = bsxfun(@rdivide,gen,sqrt(sum(gen.*gen)));

% angles
genAng = acos(dot(-V,genNorm));

% scale generators
gen = bsxfun(@rdivide,genNorm,sin(genAng));

% calculate number of transition elements
ell1 = sqrt(sum((X(:,2)-X(:,1)).^2,1));
num_trans = ceil(elltrans/ell1);
D0 = cross(L0,N0);
D1 = cross(L1,N1);

% create transition regions
X1trans = [
    linspace(X0(1),X(1,1),num_trans+1);
    linspace(X0(2),X(2,1),num_trans+1);
    linspace(X0(3),X(3,1),num_trans+1)];
gen1trans = [
    linspace(D0(1),gen(1,1),num_trans+1);
    linspace(D0(2),gen(2,1),num_trans+1);
    linspace(D0(3),gen(3,1),num_trans+1)];
X2trans = [
    linspace(X(1,end),X1(1),num_trans+1);
    linspace(X(2,end),X1(2),num_trans+1);
    linspace(X(3,end),X1(3),num_trans+1)];
gen2trans = [
    linspace(gen(1,end),D1(1),num_trans+1);
    linspace(gen(2,end),D1(2),num_trans+1);
    linspace(gen(3,end),D1(3),num_trans+1)];

% attach transition regions
X = [X1trans(:,1:end-1),X,X2trans(:,2:end)];
gen = [gen1trans(:,1:end-1),gen,gen2trans(:,2:end)];
V = [bsxfun(@times,L0,ones(3,num_trans)),V,bsxfun(@times,L1,ones(3,num_trans))];
    
% set first and last angles
genAng(1) = pi/2; genAng(end) = pi/2;

% calculate normal vector
N = cross(gen,V);

%% output figure
% cable definition
ucable = [0,0,dcable,dcable,0];
vcable = [-wcable/2,wcable/2,wcable/2,-wcable/2,-wcable/2];

% allocate cable surface
sx = zeros(length(ucable),size(X,2));
sy = zeros(length(ucable),size(X,2));
sz = zeros(length(ucable),size(X,2));

% walk over edges
for i=1:length(ucable)
    sx(i,:) = X(1,:) + gen(1,:)*vcable(i) + N(1,:)*ucable(i);
    sy(i,:) = X(2,:) + gen(2,:)*vcable(i) + N(2,:)*ucable(i);
    sz(i,:) = X(3,:) + gen(3,:)*vcable(i) + N(3,:)*ucable(i);
end

% create figure
f = figure('Color',[0.2,0.2,0.3]); 
%f = figure('Color','w'); 
ax = axes('Parent',f); 
hold(ax,'on'); axis(ax,'off'); grid(ax,'on');

% plot handles
plot3(Ph(1,:),Ph(2,:),Ph(3,:),'go-.');

% plot surface
surface(sx,sy,sz,'FaceColor','interp','EdgeColor','w');

% plote baseline
plot3(X(1,:),X(2,:),X(3,:),'r-'); 

% set axis
axis(ax,'equal');
