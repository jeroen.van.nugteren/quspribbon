# Quintic Spline Ribbons
A common problem in magnet design is the mathematical description of the cable at the coil-ends. One of such cases is the definition cloverleaf coil-coil end geometry inside the design tools for field calculation and quench analysis. While it is clearly possible to bend a stainless steel strip in such a way (see photograph), it is not obvious how to achieve this inside a computer code. This problem was solved partially in the 90s for the cosine-theta coil-ends used in the LHC, also known as constant perimeter coil-ends. However, thus far a general description has been unavailable. 

The Matlab/Octave code in this repository is an example of how quintic splines can be used to generate cable and coil generators, with a constant perimeter between a start and end coordinate and orientation. In principle all generated shapes are wind-able. Simply run main.m to start the demonstration. This method is a valuable mathematical tool for any magnet engineer.

![Cloverleaf Coil End](./figures/clovertest.png)

## Bezier Splines
A bezier spline is generally defined by a set of control points $`\overrightarrow{P_{k}}`$. The first and last are the anchor points and the intermediate points are the so-called handles. For generating the base path for the generators, a quintic bezier spline is used $`n=5`$. However, the equations provided below are general and can be used to generate any order bezier spline. The position along the spline is defined by times $`t_{j}`$, which is a regular array on the interval between $`0`$ and $`1`$. The coordinates along the spline are defined as
```math
\overrightarrow{X_{j}} = \sum_{k=0}^{n} \left(\begin{matrix}n\\k\end{matrix}\right)\overrightarrow{P_{k}}(1-t_{j})^{n-k}t_{j}^{k},
```
where $`k`$ indexes into the control points and $`j`$ indexes into the time array. The $`n`$ over $`k`$ term is the binomial coefficient defined as
```math
\left(\begin{matrix}n\\k\end{matrix}\right) = \frac{n!}{n!(n-k)!}.
```
In addition to the coordinates it is also necessary to calculate the first derivative, which points along the length of the curve. This derivative is in essence the velocity of the curve and is calculated as
```math
\overrightarrow{V_{j}} = \frac{{\partial}\overrightarrow{X_{j}}}{{\partial}t_{j}} = \sum_{k=0}^{n} \left(\begin{matrix}n\\k\end{matrix}\right) \overrightarrow{P_{k}}\frac{\left(nt_{j}-1\right)\left(1-t_{j}\right)^{n-k}t^{k-1}}{t_{j}-1},
```
Finally, also the second derivative is needed. The second derivative is essentially the acceleration and is defined as
```math
\begin{aligned}\overrightarrow{A_{j}} &= \frac{{\partial}^{2}\overrightarrow{X_{j}}}{{\partial}t_{j}^{2}} = \sum_{k=0}^{n} \left(\begin{matrix}n\\k\end{matrix}\right) \overrightarrow{P_{k}} (\left(-1+k\right)k\left(1-t_{j}\right)^{-k+n}t_{j}^{-2+k} ...\\&-  2k\left(-k+n\right)\left(1-t_{j}\right)^{-1-k+n}t_{j}^{-1+k} + \left(-1-k+n\right)\left(-k+n\right) \left(1-t_{j}\right)^{-2-k+n}t_{j}^{k}).\end{aligned}
```

## Calculating the Generators
The generators along the length of the spline define the orientation of the cable and/or coil pack. The generators form a local coordinate system along the length of the wire, $`\overrightarrow{L_{j}}`$, in the normal direction of the cable $`\overrightarrow{N_{j}}`$ and across the width of the cable $`\overrightarrow{D_{j}}`$. Note that due to the nature of the bending of a ribbon, the coordinate system is not fully orthogonal. Therefore only $`\overrightarrow{L_{j}} \cdot \overrightarrow{N_{j}} = 0`$ and $`\overrightarrow{D_{j}} \cdot \overrightarrow{N_{j}} = 0`$. However, the last dot product $`\overrightarrow{D_{j}} \cdot \overrightarrow{L_{j}}`$ is not necessarily zero. In addition the length of the transverse vector $`\overrightarrow{D_{j}}`$ is scaled to ensure that the width of the cable remains constant. The generators are calculated from the velocity and acceleration vectors as follows. First the cross product is calculated between two adjacent acceleration vectors is calculated as
```math
\overrightarrow{\gamma_{j}} =  \frac{\overrightarrow{A_{j}}}{|\overrightarrow{A_{j}}|} \times \frac{\overrightarrow{A_{j+1}}}{|\overrightarrow{A_{j+1}}|}.
```
Then the resulting vectors are interpolated at the nodes using
```math
\overrightarrow{\beta_{j}} = \left(\overrightarrow{\gamma_{j}} + \overrightarrow{\gamma_{j+1}}\right)/2. 
```
For the first and last node extrapolation is used. Then the angle between $`\overrightarrow{L_{j}}`$ and $`\overrightarrow{D_{j}}`$ is calculated with
```math
\alpha_{j} = \arccos\left(-\frac{\overrightarrow{V_{j}}}{|\overrightarrow{V_{j}}|} \cdot \frac{\overrightarrow{\beta_{j}}}{|\overrightarrow{\beta_{j}}|}\right).
```
Finally the normal vector is calculated simply using cross product as 
```math
\overrightarrow{N_{j}} = \overrightarrow{D_{j}} \times \overrightarrow{L_{j}}.
```

## Defining the Control Points
The control points are defined by the location and orientation at the start and end of the cable, where $`\overrightarrow{X_{0}}`$ is the start coordinate and $`\overrightarrow{X_{1}}`$ the end coordinate, with the respective direction vectors $`\overrightarrow{L_{0}}`$ and $`\overrightarrow{L_{1}}`$ and respective normal vectors $`\overrightarrow{N_{0}}`$ and $`\overrightarrow{N_{0}}`$. The control points are then given as
```math
\begin{matrix}
\overrightarrow{P_{0}} = \overrightarrow{X_{0}}, \hspace{0.4cm}
\overrightarrow{P_{1}} = \overrightarrow{X_{0}} + s_{1}\overrightarrow{L_{0}}, \hspace{0.4cm}
\overrightarrow{P_{2}} = \overrightarrow{X_{0}} + (s_{1}+s_{2})\overrightarrow{L_{0}} - s_{3}\overrightarrow{N_{0}},\\
\overrightarrow{P_{3}} = \overrightarrow{X_{1}} - (s_{1}+s_{2})\overrightarrow{L_{1}} - s_{4}\overrightarrow{N_{1}}, \hspace{0.4cm}
\overrightarrow{P_{4}} = \overrightarrow{X_{1}} - s_{1}\overrightarrow{L_{1}}, \hspace{0.4cm}
\overrightarrow{P_{5}} = \overrightarrow{X_{1}}.
\end{matrix}
```
These control points ensure the orientation of the cable at the start and end points of the spline. This is because the control points $`\overrightarrow{P_{0}}`$, $`\overrightarrow{P_{1}}`$ and $`\overrightarrow{P_{2}}`$ are in the plane defined by vector $`\overrightarrow{D_{0}}`$. Similar for control points $`\overrightarrow{P_{3}}`$, $`\overrightarrow{P_{4}}`$ and $`\overrightarrow{P_{5}}`$, which are in the $`\overrightarrow{D_{1}}`$ plane. As the material properties are the same at the start and end of the spline, the strengths $`s_{1}`$ and $`s_{2}`$ are used at either end. In addition under normal circumstances it seems logical to use $`s_{3}=s_{4}`$.

![definition of control points](./figures/pointdef.png)

## Examples
Generated cable section for a cloverleaf coil-end:
![cloverleaf coil end](./figures/cloverleaf.png)

Generated cable section for a block coil layer jump:
![block coil layer jump](./figures/blockjump.png)

Generated cable section for a cosine-theta coil-end:
![cosine-theta coil-end](./figures/costheta.png)

## Winding Test
Using the to be published C++ version of this code, implemented in the Foxie geometry module, a former design was generated. By winding stainless steel ribbons onto this former the code will be validated.

![Cloverleaf Coil End](./figures/cloverformer.png)

## ToDO
* Implement the extra features added later to the C++ code, mainly for improving the stability, also in these example scripts.
* See if it is possible to relate $`s_{1}`$ to $`s_{4}`$ to a fixed length of the spline.

## Author
* Jeroen van Nugteren

## License
This project is licensed under the GNU public license.

## Acknowledgments
* Bernhard Auchmann is greatly acknowledged for supplying the initial code for calculating the generators.

## References
[1] "A Primer on Bézier Curves", e-book freely available from: [https://pomax.github.io/bezierinfo/](https://pomax.github.io/bezierinfo/).

[2] S. Russenschuck, "Field Computation for Accelerator Magnets", Wiley, 2010
